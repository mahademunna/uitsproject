(function($) {
	
	"use strict";

	
    //js code for mobile menu toggle
   $(".menu-toggle").on("click", function() {
       $(this).toggleClass("is-active");
   });

   // Alart
   $('.alert').alert();
    
    // Portfolio popup
    $(".portfolio-gallery").each(function () {
        $(this).find(".popup-gallery").magnificPopup({
            type: "image",
            gallery: {
                enabled: true
            }
        });
    }); 

    $('.video-popup').magnificPopup({
        type: 'iframe',
    });

    // screenshot popup
    $(".screenshot-gallery").each(function () {
        $(this).find(".popup-screenshot").magnificPopup({
            type: "image",
            gallery: {
                enabled: true
            }
        });
    }); 

    // Promo Video Slider
    $('.promo-video-caro').owlCarousel({
        loop:true,
        dots: false,
        nav: true,
        autoplay: false,
        mouseDrag: true,
        autoplayTimeout: 10000,
        smartSpeed: 1000,
        navText: [
            '<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
        ],
        responsive:{
            0:{
                items:1,
                nav:false,
            },
            576:{
                items:1
            },
            1024:{
                items:1
            },
            1199:{
                items:1
            }
        }
    });

    // hero  Slider
    $('.hero_slider').owlCarousel({
        loop:true,
        dots: false,
        nav: true,
        autoplay: false,
        mouseDrag: true,
        autoplayTimeout: 10000,
        smartSpeed: 1000,
        navText: [
            '<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
        ],
        responsive:{
            0:{
                items:1,
                nav:false,
            },
            576:{
                items:1
            },
            1024:{
                items:1
            },
            1199:{
                items:1
            }
        }
    });
    
    // Single Campain  Slider
    $('.campain-slides').owlCarousel({
        loop:true,
        dots: false,
        nav: true,
        autoplay: false,
        mouseDrag: true,
        autoplayTimeout: 10000,
        smartSpeed: 1000,
        navText: [
            '<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
        ],
        responsive:{
            0:{
                items:1,
                nav:false,
            },
            576:{
                items:1
            },
            1024:{
                items:1
            },
            1199:{
                items:1
            }
        }
    });

    // Popular_project Slider
    $('.popular_project').owlCarousel({
        loop:true,
        dots: false,
        nav: true,
        autoplay: false,
        mouseDrag: true,
        autoplayTimeout: 10000,
        smartSpeed: 1000,
        margin: 20,
        navText: [
            '<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
        ],
        responsive:{
            0:{
                items:1,
                nav:false,
            },
            576:{
                items:1
            },
            1024:{
                items:3
            },
            1199:{
                items:3
            }
        }
    });

    // Agency Slider
    $('.agency_project').owlCarousel({
        loop:true,
        dots: false,
        nav: false,
        autoplay: false,
        mouseDrag: true,
        autoplayTimeout: 10000,
        smartSpeed: 1000,
        margin: 20,
        navText: [
            '<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
        ],
        responsive:{
            0:{
                items:1,
                nav:false,
            },
            576:{
                items:1
            },
            1024:{
                items:1
            },
            1199:{
                items:1
            }
        }
    });

    // Agency Slider2
    $('.agency_project_two').owlCarousel({
        loop:true,
        dots: false,
        nav: false,
        autoplay: false,
        mouseDrag: true,
        autoplayTimeout: 10000,
        smartSpeed: 1000,
        margin: 20,
        responsive:{
            0:{
                items:1,
                nav:false,
            },
            576:{
                items:1
            },
            1024:{
                items:4
            },
            1199:{
                items:4
            }
        }
    });

     // Brands Slider
     $('.brands').owlCarousel({
        loop:true,
        dots: false,
        autoplay: true,
        margin: 30,
        nav:false,
        responsive:{
            0:{
                items:1,                
            },
            576:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });


    // popular-works
    $('.popular-works').owlCarousel({
        loop: true,
        dots: false,
        autoplay: true,
        margin: 30,
        nav: false,
        responsive:{
            0:{
                items:1,
            },
            576:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });

        
     // Review Slider
     $('.reviews').owlCarousel({
        loop:true,
        dots: true,
        autoplay: true,
        autoplayTimeout: 5000,
        smartSpeed: 1000,
        margin: 30,
        nav:false,
        responsive:{
            0:{
                items:1,                
            },
            576:{
                items:1
            },
            1000:{
                items:2
            },
            1024:{
                items:3
            },
            1200:{
                items:3
            }
        }
    });


    // Preloader Js
    $(window).on('load', function(){
      $('.preloader').fadeOut(1000); // set duration in brackets    
    });



    //Faq area Accordion
	$('.accordion > li:eq(0) a').addClass('active').next().slideDown();

	$('.accordion a').on( 'click',function(j) {
		var dropDown = $(this).closest('li').find('p');

		$(this).closest('.accordion').find('p').not(dropDown).slideUp();

		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
		} else {
			$(this).closest('.accordion').find('a.active').removeClass('active');
			$(this).addClass('active');
		}

		dropDown.stop(false, true).slideToggle();

		j.preventDefault();
	});

	
})(jQuery);